import paho.mqtt.client as mqtt

from gpiozero import LED
from time import sleep

led = LED(17)

MQTT_HOST = "maqiatto.com"
MQTT_PORT = 1883
MQTT_KEEPALIVE_INTERVAL = 60
MQTT_TOPIC = "hyeonsig@wisoft.io/test"
MQTT_MSG = "Hello, World!"

def on_message(client, userdata, message):
    result = str(message.payload.decode("utf-8"))
    print("received message = ", result)
    
    if result == "ON":
        led.on()
    elif result == "OFF":
        led.off()
    else:
        print("Illegal message exception")

client = mqtt.Client()

client.on_message = on_message

client.username_pw_set("hyeonsig@wisoft.io", "123456")
client.connect(MQTT_HOST, MQTT_PORT, MQTT_KEEPALIVE_INTERVAL)

client.subscribe(MQTT_TOPIC)

client.loop_forever()
