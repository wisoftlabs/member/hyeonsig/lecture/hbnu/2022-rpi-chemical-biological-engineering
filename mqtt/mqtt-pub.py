import paho.mqtt.client as mqtt
import adafruit_dht
import psutil

from time import sleep

MQTT_HOST = "maqiatto.com"
MQTT_PORT = 1883
MQTT_KEEPALIVE_INTERVAL = 60
MQTT_TOPIC = "hyeonsig@wisoft.io/test"

for proc in psutil.process_iter():
    if proc.name() == 'libgpiod_pulsein64':
        proc.kill()

sensor = adafruit_dht.DHT11(4)

def on_publish(client, userdata, mid):
    print("Message published...")

client = mqtt.Client()

client.on_publish = on_publish

client.username_pw_set("hyeonsig@wisoft.io", "123456")
client.connect(MQTT_HOST, MQTT_PORT, MQTT_KEEPALIVE_INTERVAL)

while True:
    try:
        humidity = sensor.humidity
        temperature = sensor.temperature
        value = "Temp={0:0.1f}*C, Humidity={1:0.1f}%".format(temperature, humidity)
        print(value)
        client.publish(MQTT_TOPIC, value)
    except RuntimeError as error:
        sleep(2)
        continue
    except Exception as error:
        print("fff")
        sensor.exit()
    except KeyboardInterrupt:
        print("I'm done!")
        sensor.exit()
        client.disconnect()

    sleep(2)

client.loop_forever()
