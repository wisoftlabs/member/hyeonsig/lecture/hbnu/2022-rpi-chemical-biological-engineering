import adafruit_dht
import psutil
from time import sleep

for proc in psutil.process_iter():
    if proc.name() == 'libgpiod_pulsein64':
        proc.kill()

sensor = adafruit_dht.DHT11(4)


try:
    while True:
        try:
            humidity = sensor.humidity
            temperature = sensor.temperature
            print("Temp={0:0.1f}*C, Humidity={1:0.1f}%".format(temperature, humidity))
        except RuntimeError as error:
            sleep(2)
            continue
        except Exception as error:
            print("fff")
            sensor.exit()

        sleep(2)

except KeyboardInterrupt:
    print("I'm done!")
    sensor.exit()
