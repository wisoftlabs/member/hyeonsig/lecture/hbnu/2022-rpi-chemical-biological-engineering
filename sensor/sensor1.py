import adafruit_dht 

sensor = adafruit_dht.DHT11(4)

humidity = sensor.humidity
temperature = sensor.temperature

if humidity is not None and temperature is not None:
	print("Temp={0:0.1f}*C, Humidity={1:0.1f}%".format(temperature, humidity))
else:
	print("Failed to get reading. Try again!")

sensor.exit()