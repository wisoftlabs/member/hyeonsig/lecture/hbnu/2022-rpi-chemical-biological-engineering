from gpiozero import LED
from time import sleep

led = LED(17)

while True:
	print("led on")
	led.on()
	sleep(1)
	print("led off")
	led.off()
	sleep(1)
